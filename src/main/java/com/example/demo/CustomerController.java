package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class CustomerController {

	@Autowired
	private CustomerRepository repository;
	
	@Autowired
	private ObjectMapper mapper;
	
	
	@GetMapping("/customers")
	public Iterable<Customer> getAllCustomers(){
		return repository.findAll();
	}
	
	@PostMapping(value = "/customers", consumes="application/json")
	public Customer createCustomer(@RequestBody String body) throws JsonMappingException, JsonProcessingException {
		Customer newCustomer = mapper.readValue(body, Customer.class);
		newCustomer.setId("1");
		if(newCustomer.getStatus().equals("")) {
			newCustomer.setStatus("NEW");
		}
		return repository.save(newCustomer);
	}
	
	@GetMapping("/customers/{id}")
	public Customer getCustomerById(@PathVariable String id) {
	 return repository.findById(id).get();	
	}
	
	@GetMapping("/customers/count")
	public Long getAllCustomersCount() {
	  Long count = 0L;	
	  var customers = repository.findAll().iterator();
      while(customers.hasNext()) { count++; customers.next();}
      return count;
	}
	
	@GetMapping("/customers/firstNames/")
	public List<String> getAllCustomersFirstNames(){
		final List<String> names = new ArrayList<>();
		repository.findAll().forEach(customer ->{
			names.add(customer.getFirstName());
		});
		return names;
	}
}
